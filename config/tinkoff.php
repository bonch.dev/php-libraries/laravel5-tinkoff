<?php

return [
    'terminalKey'=> env('TINKOFF_TERMINAL_KEY',''),
    'secretKey'=> env('TINKOFF_SECRET_KEY',''),
    'api_url'=> env('TINKOFF_API_URL',''),
    'api_test_url'=> env('TINKOFF_API_TEST_URL',''),
];
